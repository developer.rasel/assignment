package com.sarwar.assignment.dto;

import lombok.Data;

@Data
public class Item {
    private int sku;
    private int quantity;
}